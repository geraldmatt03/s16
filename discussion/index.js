// test
console.log("Hello World!");

// Discussion proper - Repitition Control Structures
/*
	3 types of Looping Constructs
		1. While loop
		2. Do-while loop
		3. For loop
*/

/*
	While loop
		- it will take an expression/condition, if it is true, it will execute the statement
		- Syntax:
			while(expression){
				statement/s;
			};
*/

let count = 5;

while(count !== 0){
	console.log(`While: ${count}`);
	count--;
};

/*
	Expected output:
		while: 5
		while: 4
		while: 3
		while: 2
		while: 1
*/

let count1 = 0;

while(count1 !== 5){
	count1++;
	console.log(`While: ${count1}`);
};

/*
	Do-While Loop
		- Syntax:
			do{
				statement;
			}while(expression);
*/

let num1 = 1;

do{
	console.log(`Do-while: ${num1}`);
	num1++;
}while(num1 < 6);

let num2 = Number(prompt("Give a number:"));

do{
	console.log(`Do-while: ${num2}`);

	num2 += 1;
}while(num2 < 10);

/*
	for loop
		-syntax:
			for(initialization; expression; final expression){
				statement;
			}
*/

for(let num3 = 1; num3 <= 5; num3++){
	console.log(num3);
};

console.log("For loop with prompt");
let newNum = Number(prompt("Enter a number:"));
for(let num4 = newNum; num4 < 10; num4++){
	console.log(num4);
};

let numb = Number(prompt("Please insert a number:"));
console.log("Count 1-" + numb);

for(let numb1 = 1; numb1 <= numb; numb1++){
	console.log(numb1);
};

console.log("String Iteration");
let myString = "Juan";
console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

console.log("Display individual string characters:");
// myString.length == 4
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
};

let myName = "AloNzO";

console.log("Detect whether a vowel or consonant");
for(let i = 0; i < myName.length; i++){
	// if the character of the name is a vowel, instead of displaying the character, display number 3
	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		){
		console.log(3);
	}
	else{
		console.log(myName[i]);
	}
};

console.log("Continue and Break");
/*
	Continue and Break statements
		- Continue statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.
		- Break statement is used to terminate the current loop once a match has been found.
*/

for(let count2 = 0; count2 <= 20; count2++){
	if(count2 % 2 === 0){
		continue;
	}
	console.log(`Continue and Break: ${count2}`);

	if(count2 > 10){
		break;
	}
};