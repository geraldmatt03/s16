// Activity Proper
let number = Number(prompt("Please input a number:"));
number++;

for(let num = number; num--; num <= 0){
	if(num % 5 === 0 && num > 51){
		console.log(num);
		if(num % 10 === 0 && num > 51){
			console.log("The number is divisible by 10, it will be skipped");
		};
		continue;
	};

	console.log(num);

	if(num <= 50){
		console.log(`${num} is less than or equal to 50. Will not execute code.`)
		break;
	};

};


// part 2 of activity
let myString = "supercalifragilisticexpialidocious";
let consonant = 0;

for(letter = 0; letter < myString.length; letter++){
		let vowel = myString[letter];
	if(
		myString[letter].toLowerCase == "a" ||
		myString[letter].toLowerCase == "e" ||
		myString[letter].toLowerCase == "i" ||
		myString[letter].toLowerCase == "o" ||
		myString[letter].toLowerCase == "u"
		){
		console.log(vowel);
	}
	else{
		console.log(consonant = myString[letter]);
	};
};
